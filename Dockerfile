FROM mono:latest

LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

RUN apt-get update \
    && apt-get install -y \
        git \
        zip \
        unzip \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*